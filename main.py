from sklearn.neighbors import KNeighborsRegressor
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Загрузим набор данных Boston для задачи регрессии
boston = load_boston()
X = boston.data  # Признаки
y = boston.target  # Целевая переменная

# Разделим данные на тренировочный и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Создадим модель k-ближайших соседей для регрессии с числом соседей равным 3 и использованием Евклидова расстояния
knn = KNeighborsRegressor(n_neighbors=3, metric='euclidean')

# Обучим модель на тренировочных данных
knn.fit(X_train, y_train)

# Сделаем прогноз на тестовых данных
y_pred = knn.predict(X_test)

# Оценим качество модели, например, с помощью средней квадратичной ошибки
mse = mean_squared_error(y_test, y_pred)
print(f'Средняя квадратичная ошибка: {mse:.2f}')